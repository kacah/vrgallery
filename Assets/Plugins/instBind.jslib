mergeInto(LibraryManager.library, {
  getImage: function(image_id) {
    var image = window.getImage(image_id);
    var bufferSize = lengthBytesUTF8(image) + 1;
    var buffer = _malloc(bufferSize);
    stringToUTF8(image, buffer, bufferSize);
    return buffer;
  },
});
