﻿using System.Collections;
using UnityEngine;
using System.Runtime.InteropServices;

public class WallImageProcessor : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern string getImage(int imageId);

    int imageId = -100;
    bool gotImage = false;
    FadeInOut fader;
    Vector3 nextPos;
    Vector3 nextRot;

    // Use this for initialization
    void Start()
    {
        nextPos = transform.position;
        nextRot = transform.rotation.eulerAngles;
        fader = gameObject.GetComponent<FadeInOut>();
    }

    IEnumerator DrawUrl(string url)
    {
        using (WWW www = new WWW(url))
        {
            // Wait for download to complete
            yield return www;

            // assign texture
            var material = GetComponent<Renderer>().material;
            material.mainTexture = www.texture;
            //material.mainTexture = new Texture2D(www.texture.width, www.texture.height, TextureFormat.DXT1, false);
            //www.LoadImageIntoTexture(material.mainTexture as Texture2D);
            //www.Dispose();
        }
        fader.FadeIn();
    }

    // Update is called once per frame
    void Update()
    {
        if (imageId < 0 || !fader.IsHidden())
            return;

        transform.position = nextPos;
        transform.rotation = Quaternion.Euler(nextRot);

        if (!gotImage)
        {
            var image = getImage(imageId);
            // var image = "https://wallpaperbrowse.com/media/images/_89716241_thinkstockphotos-523060154.jpg";

            Debug.Log(string.Format("Image: {0}", image));
            if (image == "id_too_large")
            {
                gotImage = true;
            } else if (image != "fetching")
            {
                gotImage = true;
                StartCoroutine(DrawUrl(image));
            }
        }
    }

    public int GetImageId()
    {
        return imageId;
    }

    public void ChangeImage(int newImageId)
    {
        fader.FadeOut();
        imageId = newImageId;
        gotImage = false;
    }
    public void ChangeImage(int newImageId, Vector3 newPos, Vector3 newRot)
    {
        nextPos = newPos;
        nextRot = newRot;
        ChangeImage(newImageId);
    }
}
