﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunnelManager : MonoBehaviour {
    public float tunnelLength = 20;

    public GameObject centralTunnel;
    GameObject forwardTunnel;
    GameObject backwardTunnel;

    Vector3 initialPos;

    // Use this for initialization
    void Start () {
        forwardTunnel = Object.Instantiate(centralTunnel);
        backwardTunnel = Object.Instantiate(centralTunnel);
        initialPos = centralTunnel.transform.position;
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            Debug.Log(renderer);
        }
        RepositionTunnel();
    }

    void RepositionTunnel()
    {
        var centralTunnelOffset = (int)Mathf.Round((transform.position.z - initialPos.z) / tunnelLength);
        var centralZ = initialPos.z + tunnelLength  * centralTunnelOffset;

        centralTunnel.transform.position = new Vector3(
            initialPos.x, initialPos.y, centralZ
        );
        forwardTunnel.transform.position = new Vector3(
            initialPos.x, initialPos.y, centralZ + tunnelLength
        );
        backwardTunnel.transform.position = new Vector3(
            initialPos.x, initialPos.y, centralZ - tunnelLength
        );
    }

    // Update is called once per frame
    void Update () {
        RepositionTunnel();
    }
}
