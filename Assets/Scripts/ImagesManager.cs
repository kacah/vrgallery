﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImagesManager : MonoBehaviour {
    // number of images reserved to each side of player
    public int bufferSize = 2;
    public float gap = 10.0f;
    public float leftImageX = -5.0f;
    public float rightImageX = 5.0f;
    public Vector3 leftImageRot = new Vector3(0, 90, 0);
    public Vector3 rightImageRot = new Vector3(0, 270, 0);
    public float imageY = 4.0f;
    public float firstImageZ = -20.0f;
    public GameObject imageTemplate;

    GameObject[] images;
    int lastCenterImageOffset = -100;

	// Use this for initialization
	void Start () {
        images = new GameObject[2 * (bufferSize * 2 + 1)];
        for (int i = 0; i < images.Length; i++)
        {
            var image = Object.Instantiate(imageTemplate);
            var renderer = image.GetComponent<MeshRenderer>();
            var material = Material.Instantiate(renderer.material);
            renderer.material = material;
            images[i] = image;
        }
    }

    bool HasImageWithId(int imageId)
    {
        for (int i = 0; i < images.Length; i++)
        {
            var image = images[i];
            var imgId = image.GetComponent<WallImageProcessor>().GetImageId();
            if (imgId == imageId)
            {
                return true;
            }
        }
        return false;
    }

    void RepositionImages(int centerImageOffset)
    {
        var centerImageId = centerImageOffset * 2;
        var firstImageId = centerImageId - bufferSize * 2;
        var lastImageId = centerImageId + bufferSize * 2 + 1;

        var outdatedImages = new Stack<GameObject>();
        for (int i = 0; i < images.Length; i++)
        {
            var image = images[i];
            var imgId = image.GetComponent<WallImageProcessor>().GetImageId();
            if (imgId < firstImageId || imgId > lastImageId)
            {
                outdatedImages.Push(image);
            }
        }

        for (int imageId = firstImageId; imageId <= lastImageId; imageId++)
        {
            if (!HasImageWithId(imageId))
            {
                var image = outdatedImages.Pop();
                var imgProcessor = image.GetComponent<WallImageProcessor>();
                var imgX = leftImageX;
                var imgRot = leftImageRot;
                if (imageId % 2 == 0)
                {
                    imgX = rightImageX;
                    imgRot = rightImageRot;
                }
                var imgZ = firstImageZ + gap * (imageId / 2);
                imgProcessor.ChangeImage(imageId, new Vector3(imgX, imageY, imgZ), imgRot);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        var centerImageOffset = (int)Mathf.Round((transform.position.z - firstImageZ) / gap);
        if (lastCenterImageOffset == centerImageOffset)
            return;

        RepositionImages(centerImageOffset);
        lastCenterImageOffset = centerImageOffset;
    }
}
